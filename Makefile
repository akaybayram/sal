##
# sal
#

CC = gcc
CFLAGS = -g -Wall -Wextra
FILES = main.c
OUTPUT = binary


all: run

compile: main.c
	$(CC) $< -o $(OUTPUT) $(CFLAGS)

run:
	./$(OUTPUT)

clean:
	rm $(OUTPUT)


# end
