#ifndef SAL_H_
#define SAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

/******************************************************/
/*                                                    */
/*          Dynamic Memory Functions Defs             */
/*                                                    */
/******************************************************/

#ifndef sal_malloc
// @param size: allocated size
// @return - allocated memory or NULL if it is failed
#define sal_malloc(size) malloc(size)
#endif

#ifndef sal_free
// @param ptr: target pointer
// @param size: memory size
// @note - release allocated memory
#define sal_free(ptr, size) free(ptr)
#endif

/******************************************************/
/*                                                    */
/*               Dynamic Array Defs                   */
/*                                                    */
/******************************************************/

#define TEMEL_KAPASITE 128

typedef struct
{
    uint32_t len;
    uint32_t cap;
} SalDarrayMeta;

// @param ptr: target pointer
// @return - meta data for dynamic array
#define sal_darray_get_meta(ptr) (((SalDarrayMeta*)ptr) - 1)

// @param ptr: target pointer
// @note - initialize pointer for dynamic operations
#define sal_darray_init_start_cap(ptr, kapasite) do{\
    (ptr) = sal_malloc((kapasite) * sizeof(*(ptr)) + sizeof(SalDarrayMeta));\
    SalDarrayMeta* p = (void*)(ptr);\
    p->len = 0;\
    p->cap = (kapasite);\
    (ptr) = (void*)((SalDarrayMeta*)p + 1);\
}while(0)

// @param ptr: target pointer
// @note - initialize pointer for dynamic operations
#define sal_darray_init(ptr) sal_darray_init_start_cap((ptr), TEMEL_KAPASITE)

// @param ptr: target array
// @param val: value to push
// @note ptr maybe changed invalidates previous pointers to ptr
#define sal_darray_push(ptr, val) do{\
    sal_darray_extend_min((ptr), sal_darray_len((ptr)) + 1);\
    (ptr)[sal_darray_len((ptr))++] = val;\
}while(0)

// @param ptr: target array
// @note - deallocates array resources don't forget clean elements before this step
#define sal_darray_deinit(ptr) sal_free(sal_darray_get_meta((ptr)), sal_darray_alloc_len((ptr)))

// @param ptr: target array
// @param item: holder for elements for copy
// @note - don't use `item` pointer after for each
// @note - `int i` is defined for enumaration and updated each loop
#define sal_darray_for_each(ptr, item) for(\
    int i = 0;\
    item = (ptr)[i], i < sal_darray_len((ptr));\
    ++i)

// @param ptr: target array
// @param sptr: source array which will be appended to target
// @param size: source array size
// @note - ptr maybe changed invalidates previous pointers to ptr
#define sal_darray_append_array(ptr, sptr, size) do{\
    uint32_t birlesik_boyut = sal_darray_len((ptr)) + size;\
    sal_darray_extend_min((ptr), birlesik_boyut);\
    for(int i = 0; i < size; ++i)\
    {\
        (ptr)[sal_darray_len((ptr)) + i] = (vals)[i];\
    }\
    sal_darray_len((ptr)) = birlesik_boyut;\
}while(0)

// @param ptr: target array
// @param vals: array which will be appended to target
// @note - ptr maybe changed invalidates previous pointers to ptr
#define sal_darray_append_darray(ptr, vals) do{\
    uint32_t birlesik_boyut = sal_darray_len((ptr)) + sal_darray_len((vals));\
    sal_darray_extend_min((ptr), birlesik_boyut);\
    for(int i = 0; i < sal_darray_len((vals)); ++i)\
    {\
        (ptr)[sal_darray_len((ptr)) + i] = (vals)[i];\
    }\
    sal_darray_len((ptr)) = birlesik_boyut;\
}while(0)

// @param ptr: target array
// @param size: minimum needed size
// @note - ensures target array has needed capacity
#define sal_darray_extend_min(ptr, size) do{\
    if(sal_darray_cap(ptr) < size)\
    {\
        uint32_t curr_size = sal_darray_cap(ptr);\
        uint32_t curr_len = sal_darray_len(ptr);\
        while(curr_size <= size)\
        {\
            curr_size *= 2;\
        }\
        if(curr_size != sal_darray_cap(ptr))\
        {\
            uint8_t* mem = sal_malloc(curr_size + sizeof(SalDarrayMeta));\
            if(mem != NULL)\
            {\
                uint8_t* dst = (uint8_t*)sal_darray_get_meta(ptr);\
                for(int i = 0; i < curr_len + sizeof(SalDarrayMeta); ++i)\
                {\
                    mem[i] = dst[i];\
                }\
                sal_free(dst, sal_darray_alloc_len(dst));\
                ptr = (void*)(((SalDarrayMeta*)mem) + 1);\
            }\
            else\
            {\
                printf("extend islemi basarisiz yetersiz hafiza!");\
            }\
        }\
    }\
}while(0)

// @param ptr: target array
// @return number of elements in target array
#define sal_darray_len(ptr) (sal_darray_get_meta((ptr))->len)

// @param ptr: target array
// @return capacity of array
#define sal_darray_cap(ptr) (sal_darray_get_meta((ptr))->cap)

// @param ptr: target array
// @return overall allocated length for ptr
#define sal_darray_alloc_len(ptr) (sal_darray_cap(ptr) + sizeof(SalDarrayMeta))

// @param ptr: target array
// @return deleted element 
#define sal_darray_pop(ptr) ((ptr)[--(sal_darray_get_meta((ptr))->len)])

/******************************************************/
/*                                                    */
/*                   String Defs                      */
/*                                                    */
/******************************************************/
typedef char* sal_string;

// @param str: target str sal_string or char*
#define sal_string_init(str) do{\
    sal_darray_init((str));\
    (str)[0] = 0;\
}while(0)

// @param str: sal_string to append
// @param str2: appended string
// @note - `str` can changed while operation, invalidates previous pointers to `str`
#define sal_string_append(str, str2) do{\
    uint32_t yeni_boyut = sal_darray_len(str) + sal_darray_len(str2);\
    sal_darray_extend_min(str, yeni_boyut + 1);\
    for(int i = 0; i < sal_darray_len(str2); ++i)\
    {\
        str[sal_darray_len(str)+i] = str2[i];\
    }\
    sal_darray_len(str) = yeni_boyut;\
    str[sal_darray_len(str)] = 0;\
}while(0)

// @param str: sal_string to append
// @param ptr: ptr for chars
// @param size: size of ptr
// @note - str can changed while operation, invalidates previous pointers to str
#define sal_string_append_chararr(str, ptr, size) do{\
    sal_darray_append_array(str, ptr, size + 1);\
    str[sal_darray_len(str)] = 0;\
}while(0)

// @param str: sal_string to append
// @param cstr: null terminated C string
// @note - str can changed while operation, invalidates previous pointers to str
#define sal_string_append_cstr(str, cstr) do{\
    uint32_t boyut = strlen(cstr);\
    uint32_t yeni_boyut = boyut + sal_darray_len(str);\
    sal_darray_extend_min(str, yeni_boyut + 1);\
    for(int i = 0; i < boyut; ++i)\
    {\
        str[sal_darray_len(str)+i] = cstr[i];\
    }\
    sal_darray_get_meta(str)->len = yeni_boyut;\
    str[yeni_boyut] = 0;\
}while(0)

// @param str: NULL terminated C string
// @return - allocated dynamic string cloned from str
sal_string sal_string_from_cstr(char* str);

/******************************************************/
/*                                                    */
/*                 Dynamic Queue Defs                 */
/*                                                    */
/******************************************************/

#define sal_queue_size(queue) queue->size
#define sal_queue_cap(queue) queue->capacity

typedef struct SalQueueNode{
    void *data;
    struct SalQueueNode *next;
}SalQueueNode;

typedef struct SalQueue{
    uint32_t element_size;
    uint32_t capacity;
    uint32_t size;
    SalQueueNode *first;
    SalQueueNode *last;
}SalQueue;

SalQueue* sal_queue_new(uint32_t element_size);
void sal_queue_take(SalQueue *queue, void *data);
void sal_queue_add(SalQueue *queue, void *data);
void sal_queue_free(SalQueue *queue);
void sal_queue_setcap(SalQueue *queue, uint32_t limit);


/******************************************************/
/*                                                    */
/*             Utility Functions Defs                 */
/*                                                    */
/******************************************************/

// @param file_name: a null terminated C string or sal_string
// @return - `sal_string` to `file_name` file content or NULL
sal_string sal_read_file(char *file_name);

#ifdef SAL_IMPLEMENTATION
/* --------------------------------------------------------------------------*/
/* ----------------------- SAL implementation -------------------------*/
/* --------------------------------------------------------------------------*/
/* ------------ #define SAL_IMPLEMENTATION for function defs --------------*/

/******************************************************/
/*                                                    */
/*                   String Impl                      */
/*                                                    */
/******************************************************/
char* sal_string_from_cstr(char* str)
{
    char* res;
    uint32_t boyut = strlen(str);
    uint32_t kapasite = 
        (boyut << 2) > TEMEL_KAPASITE ? (boyut << 2): TEMEL_KAPASITE;
    sal_darray_init_start_cap(res, kapasite);
    memcpy(res, str, boyut);
    sal_darray_get_meta(res)->len = boyut;
    res[boyut] = 0;
    return res;
}

/******************************************************/
/*                                                    */
/*                 Dynamic Queue Impl                 */
/*                                                    */
/******************************************************/

SalQueue *sal_queue_new(uint32_t element_size){
    SalQueue *result = calloc(1, sizeof(SalQueue));
    result->element_size = element_size;
    result->capacity = UINT32_MAX;

    return result;
}

void sal_queue_add(SalQueue *queue, void *data){
    if(queue->size >= queue->capacity){
        return;
    }

    if(queue->size == 0){
        queue->first = calloc(1, sizeof(SalQueueNode));
        queue->last = queue->first;
        queue->first->data = calloc(1, queue->element_size);
        memcpy(queue->first->data, data, queue->element_size);
        queue->size++;
        return;
    }

    queue->last->next = calloc(1, sizeof(SalQueueNode));
    queue->last->next->data = calloc(1, queue->element_size);
    memcpy(queue->last->next->data, data, queue->element_size);
    queue->last = queue->last->next;
    queue->size++;
    return;
}

void sal_queue_take(SalQueue *queue, void *data){
    SalQueueNode *tmp;
    if(queue->size == 0){
        return;
    }

    memcpy(data, queue->first->data, queue->element_size);
    tmp = queue->first->next;
    free(queue->first->data);
    free(queue->first);
    queue->first = tmp;
    queue->size--;
}

void sal_queue_setcap(SalQueue *queue, uint32_t limit){
    queue->capacity = limit;
    return;
}

void sal_queue_free(SalQueue *queue){
    if(queue->size == 0){
        free(queue);
        return;
    }

    for(uint32_t i = 0; i < queue->size; ++i){
        SalQueueNode *tmp = queue->first->next;
        free(queue->first->data);
        free(queue->first);

        queue->first = tmp;
    }

    free(queue);
}

/******************************************************/
/*                                                    */
/*             Utility Functions Impl                 */
/*                                                    */
/******************************************************/

sal_string sal_read_file(char *file_name)
{
    sal_string result;
    size_t file_size;

    FILE *file = fopen(file_name, "rb");
    if(file == NULL)
    {
	    fprintf(stderr, "File open error!\n");
	    return NULL;
    }

    fseek(file, 0, SEEK_END);
    file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    sal_darray_init_start_cap(result, file_size + TEMEL_KAPASITE);

    if(result == NULL){
	    fprintf(stderr, "Not Enough Memory!\n");
	    return NULL;
    }

    fread(result, file_size, 1, file);
    sal_darray_len(result) = file_size;
    result[sal_darray_len(result)] = 0;
    fclose(file);
    return result;
}

#endif // SAL_IMPLEMENTATION
#endif // SAL_H_
