#include <stdio.h>
#include <stdlib.h>
#define SAL_QUEUE_IMPLEMENTATION
#include "sal.h"

struct deneme{
	int x, y;
};

int main(int argc, char *argv[]){
	(void)argc;
	(void)argv;
	SalQueue *queue = sal_queue_new(sizeof(struct deneme));
	uint32_t limit = 5;
	// for limit capacity
	queue->capacity = limit;
	// or
	sal_queue_setcap(queue, limit);

	struct deneme abc = {.x = 5, .y = 5};
	struct deneme sonuc;
	for(int i = 0; i < 10; ++i){
		sal_queue_add(queue, &abc);
		abc.x++;
		abc.y++;
	}

	for(int i = 0; i < 10; ++i){
		sal_queue_first(queue, &sonuc);

		printf("(%d , %d)\n", sonuc.x
			   , sonuc.y);

	}

	sal_queue_free(queue);
	return 0;
}
